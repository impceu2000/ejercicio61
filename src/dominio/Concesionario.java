package dominio;

import java.util.ArrayList;
import java.io.IOException;
import java.io.File;
import java.util.Scanner;
import java.io.FileWriter;


public class Concesionario{
	private ArrayList<Vehiculo> listaVehiculos = new ArrayList<>();
	private static String nombreFichero = "Vehiculos.txt";

	public double calcularValorTotal(){
		double suma = 0;
		for(Vehiculo vehiculo : listaVehiculos) suma+= vehiculo.calcularPrecioFinal();
		return suma;
	}

	public Concesionario(){
		cargarDesdeFichero();
	}

	public void annadirVehiculo(Vehiculo vehiculo){
		listaVehiculos.add(vehiculo);
		volcarAFichero();
	}

	public void mostrarVehiculos()
	{
		for(Vehiculo vehiculo : listaVehiculos) System.out.println(vehiculo.generarCadenaParaMostrar());
		System.out.println("El precio total de todos los coches es: " + calcularValorTotal());
	}

	private void volcarAFichero(){
		try{
			FileWriter fw = new FileWriter(nombreFichero);
			fw.write(this.toString());
			fw.close();
		}catch(IOException ex){
			System.err.println("Error al intentar escribir en fichero");
		}
	}

	private void cargarDesdeFichero(){
		try{
			File fichero = new File(nombreFichero);
			if (fichero.createNewFile()) {
				System.out.println("Acaba de crearse un nuevo fichero");
			} else {
				Scanner sc = new Scanner(fichero);
				while(sc.hasNext()){
					String scanner = sc.next();
					if(scanner.equals("VehiculoDeTurismo")){
					listaVehiculos.add(new VehiculoDeTurismo(sc.next(), sc.next(), Double.parseDouble( sc.next()), Integer.parseInt( sc.next())));}
					else if (scanner.equals("Furgoneta")){
					listaVehiculos.add(new Furgoneta(sc.next(), sc.next(), Double.parseDouble(sc.next()), Integer.parseInt( sc.next())));}
				}
			}
		}catch(IOException ex){
		}
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(Vehiculo vehiculo : listaVehiculos) sb.append(vehiculo + "\n");
		return sb.toString();
	}
}
