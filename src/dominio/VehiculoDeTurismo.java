package dominio;

public class VehiculoDeTurismo extends Vehiculo {
	private int numeroDePlazas;

	public VehiculoDeTurismo(String marca, String modelo, double precioBase, int numeroDePlazas) {
		super(marca, modelo, precioBase);
		this.numeroDePlazas = numeroDePlazas;
	}


	public double calcularPrecioFinal() {
		if (this.numeroDePlazas <= 5) return (this.precioBase);
		else return (this.precioBase + (this.numeroDePlazas - 5) * this.precioBase / 10);
	}
	public String generarCadenaParaMostrar() {
		return "El vehiculo de turismo de " + this.marca + " con modelo " + this.modelo + " tiene un precio de " + calcularPrecioFinal() + " euros con " + this.numeroDePlazas + " plazas.";
	}

	public String toString() {
		return "VehiculoDeTurismo " + this.marca + " " + this.modelo + " " + calcularPrecioFinal() + " " + this.numeroDePlazas + "\n";
	}
}

