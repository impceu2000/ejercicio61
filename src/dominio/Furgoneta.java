package dominio;

public class Furgoneta extends Vehiculo {
	private int capacidad;

	public Furgoneta(String marca, String modelo, double precioBase, int capacidad) {
		super(marca, modelo, precioBase);
		this.capacidad = capacidad;
	}

	public double calcularPrecioFinal() {
		 return this.precioBase * (Math.pow(this.capacidad, 1/3)/2);
	}

	public String generarCadenaParaMostrar() {
		return "La furgoneta de " + this.marca + " con modelo " + this.modelo + " tiene un precio de " + calcularPrecioFinal() + " euros con una capacidad de " + this.capacidad + " metros cúbicos.";
	}

	public String toString() {
		return "Furgoneta " +  this.marca + " " + this.modelo + " " + calcularPrecioFinal() + " " + this.capacidad + "\n";
	}

}
