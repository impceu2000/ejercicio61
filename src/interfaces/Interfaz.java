package interfaces;

import dominio.Concesionario;
import dominio.Vehiculo;
import dominio.VehiculoDeTurismo;
import dominio.Furgoneta;
import java.lang.ArrayIndexOutOfBoundsException;

public class Interfaz{
	private static Concesionario concesionario = new Concesionario(); 

	private static void mostrarAyuda(){
		System.out.println("Las instrucciones posibles son las siguientes:"); 
		System.out.println("   1. Mostrar coches: java -jar concesionario.jar show");
		System.out.println("   2. Mostrar esta ayuda: java -jar concesionario.jar help");
		System.out.println("   3. Añadir coche: java -jar concesionario.jar add.VehiculoDeTurismo <marca> <modelo> <preciobase> <numeroDePlazas>, por ejemplo, ");
		System.out.println("                    java -jar concesionario.jar add.VehiculoDeTurismo Mercedes_Benz GLC63s 10000 7");
		System.out.println("   4. Añadir coche: java -jar concesionario.jar add.Furgoneta <marca> <modelo> <preciobase> <capacidad>, por ejemplo, ");
                System.out.println("                    java -jar concesionario.jar add.Furgoneta Ford Transit 10000 350");
	}

	public static void ejecutar(String[] args){
		try
		{
			if (args[0].equalsIgnoreCase("add.VehiculoDeTurismo"))
				concesionario.annadirVehiculo(new VehiculoDeTurismo(args[1], args[2], Double.parseDouble(args[3]), Integer.parseInt(args[4])));
			else if (args[0].equalsIgnoreCase("add.Furgoneta"))
                                concesionario.annadirVehiculo(new Furgoneta(args[1], args[2], Double.parseDouble(args[3]), Integer.parseInt(args[4])));
			else if (args[0].equalsIgnoreCase("show"))
				concesionario.mostrarVehiculos();
			else if (args[0].equalsIgnoreCase("help")) mostrarAyuda();
			else mostrarAyuda();
		}catch(ArrayIndexOutOfBoundsException ex){
			mostrarAyuda();
		}
	}
}  
